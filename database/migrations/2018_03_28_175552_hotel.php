<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hotel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('hotels')) {
            Schema::create('hotels', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->unique();
                $table->string('alias')->unique();
                $table->string('logo');
                $table->string('owner');
                $table->string('street');
                $table->string('city');
                $table->string('country');
                $table->string('phone');
                $table->string('website');
                $table->string('uid');
                $table->string('bank');
                $table->string('iban');
                $table->string('bic');
                $table->timestamps();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotels');
    }
}

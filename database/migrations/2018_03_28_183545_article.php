<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Article extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('hotels')) {
            Schema::create('articles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('article_id')->unique();
                $table->string('hotel_id');
                $table->string('name');
                $table->string('price');
                $table->string('tax');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvoiceArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('hotels')) {
            Schema::create('invoice_articles', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('invoice_id');
                $table->integer('article_id');
                $table->string('name');
                $table->string('price');
                $table->string('tax');
                $table->integer('amount');
                $table->string('total_price');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice_articles');
    }
}

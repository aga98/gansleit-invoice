/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(4);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

__webpack_require__(2);
__webpack_require__(3);

/***/ }),
/* 2 */
/***/ (function(module, exports) {

$(document).ready(function () {
    var i = 0;

    $('.js-title-select').on('change', function () {
        console.log($(this).val());
        if ($(this).val() != 'Firma') {
            $(".js-title-company").addClass('d-none');
            $(".js-title-name").removeClass('d-none');
        } else {
            $(".js-title-company").removeClass('d-none');
            $(".js-title-name").addClass('d-none');
        }
    });

    $(".js-invoice-clone-btn").on("click", function () {
        i += 1;
        $('.js-invoice-clone-copy-' + i).removeClass('d-none');

        console.log(i);
    });

    $(".js-invoice-delete").on("click", function () {
        $('.js-invoice-clone-copy-' + i).addClass('d-none');

        var inputRow = '.js-invoice-clone-copy-' + i;
        $(inputRow + " input[name='name[]']").val('');
        $(inputRow + " input[name='price[]']").val('');
        $(inputRow + " input[name='tax[]']").val('');
        //$(inputRow + " input[name='quantity[]']").val('');
        $(inputRow + " input[name='price_total[]']").val('');
        $(inputRow + " input[name='article_id[]']").val('');
        i -= 1;

        console.log(i);
    });

    $('.input-daterange').datepicker({
        format: "dd.mm.yyyy",
        weekStart: 1,
        language: "de",
        todayHighlight: true
    });

    $(".js-invoice-clone-copy").on("change", '.search-article_id', function () {
        var sum = 0;
        $("input[name='price_total[]").each(function () {
            if ($(this).val() != '') {
                sum += parseInt($(this).val());
            }
        });
        $("input[name='invoice_price_total[]").val(sum);
    });

    $(".js-invoice-clone-copy").on("change", ".js-input-quantity", function () {
        var sum = 0;
        $("input[name='price_total[]").each(function () {
            if ($(this).val() != '') {
                sum += parseInt($(this).val());
            }
        });
        $("input[name='invoice_price_total[]").val(sum);
    });
    /*
        $("input[name='quantity[]").on("change", function () {
            $("input[name='article_id[]").each(function() {
                var id = $(this).val();
                var inputRow = '#inpuutRow'+id;
                var sum = 0;
                var quantity = parseInt($(inputRow + " input[name='quantity[]']").val());
                var price = parseInt($(inputRow + " input[name='price[]']").val());
                sum =  quantity * price;
                $(inputRow + " input[name='price_total[]']").val(sum);
            });
        });*/
});

/***/ }),
/* 3 */
/***/ (function(module, exports) {

$(document).ready(function () {

    var hotelAlias = $('body').data("hotel");

    $(".search-article_id").each(function () {
        $(".js-search-change-event").on("change", '.search-article_id', function () {

            var id = $(this).val();
            var input = $(this);

            $.getJSON("/" + hotelAlias + "/invoice/search-id?q=" + id, function (data) {
                var items = [];
                $.each(data, function (key, val) {
                    var inputRow = 'inpuutRow' + id;
                    $(input).parent().parent().parent().attr('id', inputRow);
                    $("#" + inputRow + " input[name='name[]']").val(val.name);
                    $("#" + inputRow + " input[name='price[]']").val(val.price);
                    $("#" + inputRow + " input[name='tax[]']").val(val.tax);
                    //$("#" + inputRow + " input[name='quantity[]']").val(1);
                    $("#" + inputRow + " input[name='price_total[]']").val(val.price);
                });
            });
        });
    });

    $(".search-name").each(function () {
        $(".js-search-change-event").on("change", '.search-name', function () {

            var id = $(this).val();
            var input = $(this);

            $.getJSON("/" + hotelAlias + "/invoice/search?q=" + id, function (data) {
                var items = [];
                $.each(data, function (key, val) {
                    var inputRow = 'inpuutRow' + val.id;
                    $(input).parent().parent().parent().attr('id', inputRow);
                    $("#" + inputRow + " input[name='article_id[]']").val(val.article_id);
                    $("#" + inputRow + " input[name='price[]']").val(val.price);
                    $("#" + inputRow + " input[name='tax[]']").val(val.tax);
                    //$("#" + inputRow + " input[name='quantity[]']").val(1);
                    $("#" + inputRow + " input[name='price_total[]']").val(val.price);
                    console.log(val.price);
                    console.log(inputRow);
                });
            });
        });
    });
});

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
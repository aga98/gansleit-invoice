$(document).ready(function() {

    var hotelAlias = $('body').data("hotel");

    $( ".search-article_id" ).each(function() {
        $(".js-search-change-event").on("change", '.search-article_id', function () {

            var id = $(this).val();
            var input = $(this);

            $.getJSON( "/" + hotelAlias +"/invoice/search-id?q=" + id, function( data ) {
                var items = [];
                $.each( data, function( key, val ) {
                    var inputRow = 'inpuutRow'+id;
                    $(input).parent().parent().parent().attr('id', inputRow);
                    $("#" + inputRow + " input[name='name[]']").val(val.name);
                    $("#" + inputRow + " input[name='price[]']").val(val.price);
                    $("#" + inputRow + " input[name='tax[]']").val(val.tax);
                    //$("#" + inputRow + " input[name='quantity[]']").val(1);
                    $("#" + inputRow + " input[name='price_total[]']").val(val.price);
                });
            });
        });
    });

    $( ".search-name" ).each(function() {
        $(".js-search-change-event").on("change", '.search-name', function () {

            var id = $(this).val();
            var input = $(this);

            $.getJSON( "/" + hotelAlias +"/invoice/search?q=" + id, function( data ) {
                var items = [];
                $.each( data, function( key, val ) {
                    var inputRow = 'inpuutRow'+val.id;
                    $(input).parent().parent().parent().attr('id', inputRow);
                    $("#" + inputRow + " input[name='article_id[]']").val(val.article_id);
                    $("#" + inputRow + " input[name='price[]']").val(val.price);
                    $("#" + inputRow + " input[name='tax[]']").val(val.tax);
                    //$("#" + inputRow + " input[name='quantity[]']").val(1);
                    $("#" + inputRow + " input[name='price_total[]']").val(val.price);
                    console.log(val.price);
                    console.log(inputRow);
                });
            });
        });
    });
});


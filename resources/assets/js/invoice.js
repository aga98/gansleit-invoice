$(document).ready(function () {
    var i = 0;

    $('.js-title-select').on('change', function() {
        console.log($(this).val());
        if($(this).val() != 'Firma') {
            $(".js-title-company").addClass('d-none');
            $(".js-title-name").removeClass('d-none');
        }
        else {
            $(".js-title-company").removeClass('d-none');
            $(".js-title-name").addClass('d-none');
        }    })

    $(".js-invoice-clone-btn").on("click", function () {
        i += 1;
        $('.js-invoice-clone-copy-' + i).removeClass('d-none');

        console.log(i);
    });

    $(".js-invoice-delete").on("click", function () {
        $('.js-invoice-clone-copy-' + i).addClass('d-none');

        var inputRow = '.js-invoice-clone-copy-' + i;
        $(inputRow + " input[name='name[]']").val('');
        $(inputRow + " input[name='price[]']").val('');
        $(inputRow + " input[name='tax[]']").val('');
        //$(inputRow + " input[name='quantity[]']").val('');
        $(inputRow + " input[name='price_total[]']").val('');
        $(inputRow + " input[name='article_id[]']").val('');
        i -= 1;

        console.log(i);
    });

    $('.input-daterange').datepicker({
        format: "dd.mm.yyyy",
        weekStart: 1,
        language: "de",
        todayHighlight: true
    });


    $(".js-invoice-clone-copy").on("change", '.search-article_id', function () {
        var sum = 0;
        $("input[name='price_total[]").each(function() {
            if( $(this).val() != '' ) {
                sum += parseInt($(this).val());
            }
        });
        $("input[name='invoice_price_total[]").val(sum);
    });

    $(".js-invoice-clone-copy").on("change", ".js-input-quantity", function () {
        var sum = 0;
        $("input[name='price_total[]").each(function() {
            if( $(this).val() != '' ) {
                sum += parseInt($(this).val());
            }
        });
        $("input[name='invoice_price_total[]").val(sum);
    });
/*
    $("input[name='quantity[]").on("change", function () {
        $("input[name='article_id[]").each(function() {
            var id = $(this).val();
            var inputRow = '#inpuutRow'+id;
            var sum = 0;
            var quantity = parseInt($(inputRow + " input[name='quantity[]']").val());
            var price = parseInt($(inputRow + " input[name='price[]']").val());
            sum =  quantity * price;
            $(inputRow + " input[name='price_total[]']").val(sum);
        });
    });*/
});


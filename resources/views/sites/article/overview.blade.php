@extends('layouts.app')

@section('content')

    <div class="modal fade" id="createArticle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Neuer Artikel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/{{$hotel->alias}}/article/create" method="post" autocomplete="off">
                    @include('sites.article.form')
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}">Informationen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/customer">Gäste</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/{{$hotel->alias}}/article">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/invoice">Rechnungen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/invoice/create">Rechnung erstellen</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Artikel</h2>
                    </div>
                    <div class="col-sm-6">
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                data-target="#createArticle">Artikel hinzufügen
                        </button>
                    </div>
                    <div class="col-sm-12">
                        <hr/>
                        @if(app('request')->input('message') == 'delete')
                            <div class="alert alert-success">
                                Artikel wurde gelöscht!
                            </div>
                        @endif
                        @if(app('request')->input('message') == 'create')
                            <div class="alert alert-success">
                                <strong>Yuhuu!!</strong> Artikel wurde angelegt!
                            </div>
                        @endif
                        @if(app('request')->input('message') == 'update')
                            <div class="alert alert-success">
                                <strong>Yuhuu!!</strong> Artikel wurde aktualisiert!
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">Artikel Nummer</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Preis</th>
                                            <th scope="col">Mwst.</th>
                                            <th scope="col">Bearbeiten</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($articles as $article)
                                        <tr>
                                            <th scope="row">{{ $article->article_id }}</th>
                                            <td>{{ $article->name }}</td>
                                            <td>{{ $article->price }}</td>
                                            <td>{{ $article->tax }}</td>
                                            <td><a href="" data-toggle="modal" data-target="#edit{{$article->id}}">
                                                    Artikel bearbeiten
                                                </a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="edit{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="edit{{$article->id}}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">{{$article->name}} bearbeiten</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="/{{$hotel->alias}}/article/update-{{$article->id}}" method="post" autocomplete="off">
                                                        @include('sites.article.form')
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{ csrf_field() }}
<div class="modal-body">
    <div class="row mb-2">
        <div class="col-sm-12">
            <label class="col-form-label">Name:</label>
            <input type="text" class="form-control" name="name" autofill="false" value="{{$article->name or ''}}">
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-sm-6">
            <label class="col-form-label">Preis:</label>
            <input type="number" class="form-control" name="price" step="0.01" value="{{$article->price or ''}}">
        </div>
        <div class="col-sm-6">
            <label class="col-form-label">Artikel Nummer:</label>
            <input type="number" class="form-control" name="article_id" value="{{$article->article_id or ''}}">
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-sm-6">
            <label class="col-form-label">Zum Versteuern:</label>
            <input type="number" class="form-control" name="tax_first_percent" step="0" value="{{$article->tax_first_percent or 0}}">
        </div>
        <div class="col-sm-6">
            <label class="col-form-label">Steuer:</label>
            <select class="form-control" name="tax">
                <option value="0">0% Mwst.</option>
                <option value="10">10% Mwst.</option>
                <option value="13">13% Mwst.</option>
                <option value="20">20% Mwst.</option>
                @if(isset($article->tax)) <option value="{{$article->tax}}" selected="">{{$article->tax}}% Mwst.</option> @endif
            </select>
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-sm-6">
            <label class="col-form-label">Zum Versteuern:</label>
            <input type="number" class="form-control" name="tax_second_percent" step="0" value="{{$article->tax_second_percent or 0}}">
        </div>
        <div class="col-sm-6">
            <label class="col-form-label">Steuer:</label>
            <select class="form-control" name="tax_second_value">
                <option value="0">0% Mwst.</option>
                <option value="10">10% Mwst.</option>
                <option value="13">13% Mwst.</option>
                <option value="20">20% Mwst.</option>
                @if(isset($article->tax_second_value)) <option value="{{$article->tax_second_value}}" selected="">{{$article->tax_second_value}}% Mwst.</option> @endif
            </select>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-check">
            <input @if(!isset($article->only_single_hotel)) checked="" @elseif($article->only_single_hotel == true) checked="" @endif class="form-check-input" type="checkbox" id="gridCheck{{$article->id or '0'}}"
                   name="only_single_hotel" value="1">
            <label class="form-check-label" for="gridCheck{{$article->id or '0'}}" >
                Nur für {{$hotel->name}}
            </label>
        </div>
    </div>
    <input type="hidden" name="hotel_id" value="{{$hotel->id}}"/>
</div>
<div class="modal-footer">
    @if(isset($article))
        <a href="/{{$hotel->alias}}/article/destroy-{{$article->id}}" class="btn btn-danger pull-left">Löschen</a>
    @endif
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
    <button type="submit" class="btn btn-primary">Speichern</button>
</div>
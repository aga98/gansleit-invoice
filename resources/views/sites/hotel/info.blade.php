@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" href="/{{$hotel->alias}}">Informationen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/customer">Gäste</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/article">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/invoice">Rechnungen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/invoice/create">Rechnung erstellen</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9">
                <form method="post">
                    {{csrf_field()}}
                    <div class="card mb-4">
                        <h5 class="card-header">{{$hotel->name}}</h5>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" value="{{$hotel->name}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Besitzer</label>
                                    <input type="text" class="form-control" name="owner" value="{{$hotel->owner}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>UID Nummer</label>
                                    <input type="text" class="form-control" name="uid" value="{{$hotel->uid}}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <button type="submit" class="btn btn-success float-right">Speichern</button>
                        </div>
                    </div>

                    <div class="card mb-4">
                        <h5 class="card-header">Adresse</h5>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Straße</label>
                                    <input type="text" class="form-control" name="street" value="{{$hotel->street}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Stadt</label>
                                    <input type="text" class="form-control" name="city" value="{{$hotel->city}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Land</label>
                                    <input type="text" class="form-control" name="country" value="{{$hotel->country}}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <button type="submit" class="btn btn-success float-right">Speichern</button>
                        </div>
                    </div>

                    <div class="card mb-4">
                        <h5 class="card-header">Kontakt</h5>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Telefon</label>
                                    <input type="text" class="form-control" name="phone" value="{{$hotel->phone}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Website</label>
                                    <input type="text" class="form-control" name="website" value="{{$hotel->website}}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <button type="submit" class="btn btn-success float-right">Speichern</button>
                        </div>
                    </div>

                    <div class="card">
                        <h5 class="card-header">Bankverbindung</h5>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Bank</label>
                                    <input type="text" class="form-control" name="bank" value="{{$hotel->bank}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>IBAN</label>
                                    <input type="text" class="form-control" name="iban" value="{{$hotel->iban}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>BIC</label>
                                    <input type="text" class="form-control" name="bic" value="{{$hotel->bic}}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <button type="submit" class="btn btn-success float-right">Speichern</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container" style="height: calc(100vh - 150px);display: flex;justify-content: center;align-items: center;">
        <div class="row">
            <div class="col-md-12 justify-content-md-center">
                <h1 class="h1 justify-content-md-center d-flex mb-4">Rechnung {{$invoice->invoice_id}}</h1>
            </div>
            <div class="col-md-5 offset-md-1">
                <div class="card">
                    <div class="card-body justify-content-center flex-column d-flex" style="text-align: center;">
                        <h5 class="card-title">Drucken</h5>
                        <a href="/{{$hotel->alias}}/invoice/view/{{$invoice->invoice_id}}" class="btn btn-primary" target="_blank">Weiter</a>
                    </div>
                </div>
            </div>

            <div class="col-md-5 offset-md-0">
                <div class="card justify-content-md-center flex-column d-flex">
                    <div class="card-body justify-content-md-center flex-column d-flex" style="text-align: center;">
                        <h5 class="card-title">Neue Rechnung</h5>
                        <a href="/{{$hotel->alias}}/invoice/create" class="btn btn-primary">Weiter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

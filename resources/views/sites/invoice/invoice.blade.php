<!DOCTYPE html>
<html>
<head>
    <title>Rechnung - PDF</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        @page {
            footer: page-footer;
            header: page-header;
        }
    </style>
</head>
<body>
<htmlpageheader name="page-header">
    <div class="container">
        <div class="col-md-4 col-md-offset-4 text-center">
            <img src="/img/{{$hotel->logo}}" style="width: 200px; color: #d0a553;">
        </div>
    </div>
</htmlpageheader>
<htmlpagefooter name="page-footer">
    @if($hotel->id == 1)
        <div class="text-center" style="font-size: 12px; color: #d2a44c;">
    @else
        <div class="text-center" style="font-size: 12px; color: #958a54;">
    @endif
        {{$hotel->name}}
        • {{$hotel->owner}} <br/>
        {{$hotel->street}}
        • {{$hotel->city}}
        • {{$hotel->country}}<br/>
        Tel: {{$hotel->phone}}
        • {{$hotel->website}}<br/><br/>
        UID-NR {{$hotel->uid}}
        • Bankverbindung: {{$hotel->bank}}
        • IBAN: {{$hotel->iban}}
        • BIC: {{$hotel->bic}}
    </div>
</htmlpagefooter>
<div class="container">

    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <table class="table" style="margin-bottom: 40px; font-size: 16px;">
        <tbody>
        <tr>
            <td>{{$customer->sex}}</td>
            <td rowspan="5" style="text-align: right;"><img width="100"
                                                            src="https://chart.googleapis.com/chart?cht=qr&chl={{ env('APP_URL') }}public/{{$hotel->alias}}/invoice-{{$invoice->invoice_id * 12345678}}&chs=100x100&chld=L|0">
            </td>
        </tr>
        <tr>
            <td>{{$customer->full_name}}</td>
        </tr>
        <tr>
            <td>{{$customer->street}}</td>
        </tr>
        <tr>
            <td>{{$customer->plz}} {{$customer->city}} </td>
        </tr>
        <tr>
            <td>{{$customer->country}} </td>
        </tr>
        </tbody>
    </table>

    <table class="table" style="margin-bottom: 40px;">
        <tbody>
        <tr>
            <td style="font-size: 20px;">
                <strong style="font-weight: bold;">Rechnung:</strong> {{$invoice->invoice_id}} <br/>
                <span s tyle=" font-size: 13px;"><strong style="font-weight: bold;">Aufenthaltsdauer:</strong> <small>{{$invoice->date_from}} bis {{$invoice->date_to}}</small></span>
            </td>
            <td style="text-align:right;">
                <strong style="font-weight: bold;">Rechnungsdatum:</strong> {{ \Carbon\Carbon::parse($invoice->created_at)->format('d.m.Y')}}
                <br/>
                <strong style="font-weight: bold;">UID Nummer:</strong> {{$hotel->uid}}
            </td>
        </tr>
        </tbody>
    </table>

    <table class="table">
        <thead>
        <tr>
            <th style="padding: 10px 0;">Artikel</th>
            <th style="padding: 10px;">Menge</th>
            <th style="padding: 10px;">Brutto-Einzel</th>
            <th style="padding: 10px;">MWST</th>
            <th style="padding: 10px;">Positions-Brutto</th>
        </tr>
        </thead>
        <tbody>
        @php ($total_price = 0)
        @foreach($articles as $item)
            <tr>
                <td style="padding: 10px 0; width: 220px; border-top: 1px solid #cecece;">{{$item->name}}</td>
                <td style="padding: 10px; border-top: 1px solid #cecece;">{{$item->amount}}</td>
                <td style="padding: 10px; border-top: 1px solid #cecece;">{{number_format($item->price,2)}} €</td>
                <td style="padding: 10px; border-top: 1px solid #cecece;">{{$item->tax}}%</td>
                <td style="padding: 10px; border-top: 1px solid #cecece;">{{number_format($item->total_price,2)}} €</td>
            </tr>
            @php ($total_price += $item->total_price)
        @endforeach
        </tbody>
    </table>
    <table class="table" style="margin-top: 10px; font-size: 16px;">
        <tbody>
        @if(number_format($invoice->aconto,2) != '0.00')
            <tr>
                <td style="padding: 5px 0; font-size: 16px; text-align: right;"><strong style="font-weight: bold;">Zwischensumme
                        (inkl. Mwst.): </strong></td>
                <td style="font-size: 16px; width: 120px; padding: 5px 0; padding-left: 20px; text-align: right;">
                    <strong style="font-weight: bold;">{{number_format($total_price,2)}} €</strong></td>
            </tr>
            <tr>
                <td style="padding: 5px 0; font-size: 16px; text-align: right; border-bottom: 1px solid #cecece;">
                    <strong style="font-weight: bold;">Aconto Betrag: </strong></td>
                <td style="font-size: 16px; width: 120px; padding: 5px 0; padding-left: 20px; text-align: right; border-bottom: 1px solid #cecece;">
                    <strong style="font-weight: bold;">- {{number_format($invoice->aconto,2)}} €</strong></td>
            </tr>
            <tr>
                <td style="padding: 5px 0; font-size: 16px; text-align: right;"><strong style="font-weight: bold;">Gesamtpreis
                        (inkl. Mwst.): </strong></td>
                <td style="font-size: 16px; width: 120px; padding: 5px 0; padding-left: 20px; text-align: right;">
                    <strong style="font-weight: bold;">{{number_format($total_price - $invoice->aconto,2)}} €</strong>
                </td>
            </tr>
        @else
            <tr>
                <td style="padding: 5px 0; font-size: 16px; text-align: right;"><strong style="font-weight: bold;">Gesamtpreis
                        <small style="font-size: 11px;">(inkl. Mwst.)</small>: </strong></td>
                <td style="font-size: 16px; width: 120px; padding: 5px 0; padding-left: 20px; text-align: right;">
                    <strong style="font-weight: bold;">{{number_format($total_price,2)}} €</strong></td>
            </tr>
        @endif
        </tbody>
    </table>
    <table class="table pull-right" style="margin-top: 10px; margin-left: auto; text-align: right; font-size: 16px; width: 350px; float: right;">
        <tbody>
        <tr style="text-align: right;">
            <th style="padding: 10px 0; width: 100px; text-align: right;">Mwst. %</th>
            <th style="padding: 10px 0; width: 100px; text-align: right;">Netto</th>
            <th style="padding: 10px 0; width: 100px; text-align: right;">Mwst. €</th>
            <th style="padding: 10px 0; width: 100px; text-align: right;">Brutto</th>
        </tr>
        @php ($taxes = [20, 13, 10, 0])
        @php ($kurtaxe_value = 0)
        @php ($sumNetto = 0)
        @php ($sumMWST = 0)
        @php ($sumBrutto = 0)
        @foreach($taxes as $tax_value)
            @php ($tax = $tax_value)
            @php ($sum = 0)

            @foreach($articles as $item)
                @if($tax == $item->tax)
                    @php ($sum += ((($item->price) * $item->amount) / 100) * $item->tax_first_percent )
                @elseif($tax == $item->tax_second_value and $item->tax_second_value != 0)
                    @php ($sum += ((($item->price) * $item->amount) / 100) * $item->tax_second_percent )
                @endif
            @endforeach

            <tr>
                <td style="padding-bottom: 5px;">{{$tax_value}}%</td>
                <td style="padding-bottom: 5px;">{{ number_format(($sum - ($sum / (100 + $tax_value)) * $tax_value),2) }} €</td>
                <td style="padding-bottom: 5px;">{{number_format(($sum / (100 + $tax_value)) * $tax_value,2)}} €</td>
                <td style="padding-bottom: 5px;">{{number_format($sum,2)}} €</td>
            </tr>

            @php ($sumNetto += $sum - ($sum / (100 + $tax_value)) * $tax_value)
            @php ($sumMWST += $sum / (100 + $tax_value) * $tax_value)
            @php ($sumBrutto += $sum)
        @endforeach
            <tr>
                <td style="border-top: 1px solid #cecece; padding-top: 5px;"></td>
                <td style="border-top: 1px solid #cecece; padding-top: 5px;">{{number_format($sumNetto,2)}} €</td>
                <td style="border-top: 1px solid #cecece; padding-top: 5px;">{{number_format($sumMWST,2)}} €</td>
                <td style="border-top: 1px solid #cecece; padding-top: 5px;">{{number_format($sumBrutto,2)}} €</td>
            </tr>
        </tbody>
    </table>
</div>
</body>
</html>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}">Informationen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/customer">Gäste</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/article">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/{{$hotel->alias}}/invoice">Rechnungen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/invoice/create">Rechnung erstellen</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Alle Rechnungen</h2>
                    </div>
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-12">
                        <hr/>
                    </div>
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Rechnungsnummer</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Erstellt am</th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($invoices as $invoice)
                                        <tr>
                                            <th scope="row">{{ $invoice->invoice_id }}</th>
                                            <td>{{ $invoice->customer->full_name }}</td>
                                            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->format('d.m.Y')}}</td>
                                            <td>
                                                <a target="_blank" href="/{{$hotel->alias}}/invoice/view/{{ $invoice->invoice_id }}">Rechnung ansehen</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mb-4">
                <ul class="nav nav-pills justify-content-left">
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}">Informationen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/customer">Gäste</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/article">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/invoice">Rechnungen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/{{$hotel->alias}}/invoice/create">Rechnung erstellen</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12">
                <form id="invoicecreate">
                    <input type="hidden" name="hotel_id" value="{{$hotel->id}}">
                    {{csrf_field()}}
                    <div class="card mb-4">
                        <h5 class="card-header">Gast</h5>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label>Anrede</label>
                                    <select class="form-control js-title-select" name="sex">
                                        <option value="Herr" @if(app('request')->input('sex') == 'Herr') selected @endif>Herr</option>
                                        <option value="Frau" @if(app('request')->input('sex') == 'Frau') selected @endif>Frau</option>
                                        <option value="Firma" @if(app('request')->input('sex') == 'Firma') selected @endif>Firma</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 js-title-company d-none">
                                    <label>Firma</label>
                                    <input type="text" class="form-control" name="company" required="required" value="{{ app('request')->input('company')}}" autocomplete="false">
                                </div>
                                <div class="form-group col-md-3 js-title-name">
                                    <label>Vorname</label>
                                    <input type="text" class="form-control" name="surname" required="required" value="{{ app('request')->input('surname')}}" autocomplete="false">
                                </div>
                                <div class="form-group col-md-3 js-title-name">
                                    <label>Nachname</label>
                                    <input type="text" class="form-control" name="firstname" required="required" value="{{ app('request')->input('firstname')}}" autocomplete="false">
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Meldescheinnummer</label>
                                    <input type="text" class="form-control" name="registration_card_number" required="required" autocomplete="false">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label>Land</label>
                                    <input type="text" class="form-control" name="country" required="required" value="{{ app('request')->input('country')}}" autocomplete="false">
                                </div>
                                <div class="form-group col-md-3">
                                    <label>PLZ</label>
                                    <input type="text" class="form-control" name="plz" required="required" value="{{ app('request')->input('plz')}}" autocomplete="false">
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Stadt</label>
                                    <input type="text" class="form-control" name="city" required="required" value="{{ app('request')->input('city')}}" autocomplete="false">
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Straße + Hausnummer</label>
                                    <input type="text" class="form-control" name="street" required="required" value="{{ app('request')->input('street')}}" autocomplete="false">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-4">
                        <h5 class="card-header">Sonstiges</h5>
                        <div class="card-body">
                            <div class="form-row">

                                <div class="form-group col-md-6 display-none">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Anreise</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Abreise</label>
                                        </div>
                                    </div>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="form-control datepicker-from" name="start" required/>
                                        <span class="input-group-addon" style="padding: 10px;">bis</span>
                                        <input type="text" class="form-control datepicker-to" name="end" required/>
                                    </div>
                                </div>{{--
                                <div class="form-group col-md-2">
                                    <label>Personen</label>
                                    <input type="number" step="0" class="form-control" name="persons" value="1">
                                </div>--}}
                                <div class="form-group col-md-4">
                                    <label>Aconto-Betrag</label>
                                    <input type="number" step="0.01" class="form-control" name="aconto" value="0.00">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <h5 class="card-header">Rechnung #{{sprintf("%04s", $hotel->invoiceId)}}</h5>
                        <div class="card-body">
                            <div class="js-invoice-clone-insert">
                                @for ($i = 0; $i < 14; $i++)
                                    <div class="form-row js-invoice-clone-copy js-invoice-clone-copy-{{$i}} @if($i != 0) d-none @endif">
                                        <div class="form-group col-md-2 js-search-change-event">
                                            <label>Artikel Nr.</label>
                                            <input id="search-article_id" type="search" class="form-control search-article_id" name="article_id[]" autocomplete="false">
                                        </div>
                                        <div class="form-group col-md-7 js-search-change-event">
                                            <label style="display: block;">Bezeichnung</label>
                                            <input id="search-name" type="search" class="form-control search-name" name="name[]" autocomplete="false" style="display: block;">
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label>Menge</label>
                                            <input type="number" class="form-control js-input-quantity" name="quantity[]" value="0">
                                        </div>
                                        <div class="form-group col-md-1 d-none">
                                            <label>Rabatt <small>in %</small></label>
                                            <input type="number" class="form-control js-input-quantity" name="discount[]" value="0">
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label>Einzelpreis</label>
                                            <input type="text" step="0.01" class="form-control" name="price[]">
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label>Mwst.</label>
                                            <input type="text" class="form-control" name="tax[]">
                                        </div>
                                    </div>
                                @endfor
                            </div>
                            <div class="clearfix"></div>
                            <button type="button" class="js-invoice-clone-btn btn btn-primary">+ neuer Artikel</button>
                            <button type="button" class="text-right btn btn-danger js-invoice-delete">- Löschen</button>
                            <div class="clearfix"></div>
                            <hr/>
                        </div>
                    </div>
                    <hr />
                    <button type="button" id="create" class="btn btn-primary">Rechnung drucken</button>
                    <button type="button" id="preview" class="btn btn-info">Vorschau Rechnung</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            var form = document.getElementById('invoicecreate');

            form.onsubmit = function() {
                form.target = '_self';
                form.method = 'post';
            };

            document.getElementById('create').onclick = function() {
                form.target = '_self';
                form.method = 'post';
                form.action = '/{{$hotel->alias}}/invoice/create';
                form.submit();
            }

            document.getElementById('preview').onclick = function() {
                form.target = '_blank';
                form.method = 'get';
                form.action = '/{{$hotel->alias}}/invoice/preview';
                form.submit();
            }

            $('#datepicker').on('changeDate', function() {

                var from = $('.datepicker-from').datepicker({ dateFormat: 'yy-mm-dd' }).val();
                var to = $('.datepicker-to').datepicker({ dateFormat: 'yy-mm-dd' }).val();

                console.log('Von: ' + from);
                console.log('Bis: ' + to);

            });

            var bloodhound = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '/{{$hotel->alias}}/invoice/search?q=%QUERY%',
                    wildcard: '%QUERY%'
                },
            });

            var bloodhoundID = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '/{{$hotel->alias}}/invoice/search-id?q=%QUERY%',
                    wildcard: '%QUERY%'
                },
            });


            $( ".search-name" ).each(function( index ) {
                $(this).typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'users',
                    source: bloodhound,
                    display: function (data) {
                        return data.name  //Input value to be set when you select a suggestion.
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                        ],
                        header: [
                            '<div class="list-group search-results-dropdown">'
                        ],
                        suggestion: function (data) {
                            return '<div style="font-weight:normal;" class="list-group-item js-search-name-result" data-name="' + data.name + '">' + data.name + '</div></div>'
                        }
                    }
                });
            });

            $( ".search-article_id" ).each(function( index ) {
                $(this).typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'users',
                    source: bloodhoundID,
                    display: function (data) {
                        return data.article_id  //Input value to be set when you select a suggestion.
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                        ],
                        header: [
                            '<div class="list-group search-results-dropdown">'
                        ],
                        suggestion: function (data) {
                            return '<div style="font-weight:normal;" class="list-group-item js-search-article_id-result" data-name="' + data.name + '">' + data.name + '</div></div>'
                        }
                    }
                });
            });
        });
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}">Informationen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/article">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/customer">Gäste</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/invoice">Rechnungen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/{{$hotel->alias}}/invoice/create">Rechnung erstellen</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Suche</h2>
                    </div>
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-12">
                        <hr/>
                    </div>
                    <div class="col-sm-12">

                        <div class="list-group">
                            @foreach($searchResult['customer'] as $customer)
                                <a href="/{{$hotel->alias}}/invoice/create?sex={{$customer->sex}}&firstname={{$customer->firstname}}&surname={{$customer->surname}}&street={{$customer->street}}&city={{$customer->city}}&plz={{$customer->plz}}&country={{$customer->country}}" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">{{$customer->full_name}}</h5>
                                        <small>Erstellt am: {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $customer->created_at)->format('d.m.Y')}}</small>
                                    </div>
                                    <p class="mb-1">
                                        Meldescheinnummer: {{$customer->registration_card_number}}<br/>
                                        <small>
                                            {{$customer->street}},
                                            {{$customer->city}}
                                            {{$customer->plz}},
                                            {{$customer->country}}
                                        </small>
                                    </p>
                                </a>
                            @endforeach

                            @foreach($searchResult['invoice'] as $invoice)
                                <a href="/{{$hotel->alias}}/invoice/view/{{$invoice->invoice_id}}" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">{{$invoice->invoice_id}}</h5>
                                        <small>Erstellt am: {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->format('d.m.Y')}}</small>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

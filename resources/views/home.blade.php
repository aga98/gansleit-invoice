@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Hotel Gänsleit</h5>
                    <a href="/hotel-gaensleit/invoice/create" class="btn btn-primary">Rechnung erstellen</a>
                    <a href="/hotel-gaensleit/article" class="btn btn-link">Artikel bearbeiten</a>
                    <a href="/hotel-gaensleit" class="btn btn-link">Information bearbeiten</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Appartementhaus Echt Woods</h5>
                    <a href="/appartementhaus-echt-woods/invoice/create" class="btn btn-primary">Rechnung erstellen</a>
                    <a href="/appartementhaus-echt-woods/article" class="btn btn-link">Artikel bearbeiten</a>
                    <a href="/appartementhaus-echt-woods" class="btn btn-link">Information bearbeiten</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 'hotel_id', 'name', 'price', 'tax', 'tax_first_percent', 'tax_second_percent', 'tax_second_value', 'only_single_hotel'
    ];
}

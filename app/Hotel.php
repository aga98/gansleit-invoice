<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'alias', 'owner', 'street', 'city', 'country', 'phone', 'website', 'uid', 'bank', 'iban', 'bic',
    ];
}

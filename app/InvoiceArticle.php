<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceArticle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 'hotel_id', 'name', 'price', 'hotel_id', 'tax', 'kurtaxe', 'tax_second_value', 'tax_first_percent', 'tax_second_percent'
    ];
}

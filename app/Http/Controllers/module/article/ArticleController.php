<?php

namespace App\Http\Controllers\module\article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Article;
use DB;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function overview($hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();
        $articles = DB::table('articles')
                        ->where('hotel_id', $hotel->id)
                        ->orWhere('only_single_hotel', 0)
                        ->orderBy('name')
                        ->get();

        return view('sites.article.overview', compact('hotel'), compact('articles'));
    }

    public function create(Request $request, $hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();

        $article = new Article;

        $article->hotel_id = $request['hotel_id'];
        $article->article_id = $request['article_id'];
        $article->name = $request['name'];
        $article->tax = $request['tax'];
        $article->price = $request['price'];
        $article->tax_second_value = $request['tax_second_value'];
        $article->tax_second_percent = $request['tax_second_percent'];
        if($request['only_single_hotel'] == null ) {
            $request['only_single_hotel'] = 0;
        }
        $article->only_single_hotel = $request['only_single_hotel'];

        $article->save();

        return redirect($hotel->alias.'/article?message=create');
    }

    public function update(Request $request, $hotelAlias, $articleId)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();

        $article = Article::findOrfail($articleId);

        $article->hotel_id = $request['hotel_id'];
        $article->article_id = $request['article_id'];
        $article->name = $request['name'];
        $article->tax = $request['tax'];
        $article->price = $request['price'];
        $article->tax_second_value = $request['tax_second_value'];
        $article->tax_second_percent = $request['tax_second_percent'];
        $article->tax_first_percent = $request['tax_first_percent'];
        if($request['only_single_hotel'] == null ) {
            $request['only_single_hotel'] = 0;
        }
        $article->only_single_hotel = $request['only_single_hotel'];

        $article->save();

        return redirect($hotel->alias.'/article?message=update');
    }

    public function destroy(Request $request, $hotelAlias, $articleId)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();

        Article::destroy($articleId);

        return redirect($hotel->alias.'/article?message=delete');
    }
}

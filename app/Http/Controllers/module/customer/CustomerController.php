<?php

namespace App\Http\Controllers\module\customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use DB;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function viewAll($hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();
        $customers = DB::table('customers')->where('hotel_id', $hotel->id)->orderBy('created_at', 'desc')->paginate(40);

        return view('sites.customers.viewall', compact('hotel'), compact('customers'));
    }

    public function create($request)
    {
        $customer = new Customer;

        $customer->sex = $request['sex'];
        $customer->hotel_id = $request['hotel_id'];
        $customer->company = $request['company'];
        $customer->surname = $request['surname'];
        $customer->firstname = $request['firstname'];
        $customer->full_name = $request['firstname']." ".$request['surname'];
        if(!empty($request['company'])) {
            $customer->full_name = $request['company'];
        }
        $customer->plz = $request['plz'];
        $customer->country = $request['country'];
        $customer->city = $request['city'];
        $customer->street = $request['street'];
        $customer->registration_card_number = $request['registration_card_number'];

        $customer->save();

        return $customer->id;
    }
}

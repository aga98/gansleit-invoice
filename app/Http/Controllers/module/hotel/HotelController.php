<?php

namespace App\Http\Controllers\module\hotel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Hotel;
use DB;

class HotelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();

        return view('sites.hotel.info', compact('hotel'));
    }

    public function update($hotelAlias, Request $request)
    {
        $hotel = Hotel::where('alias', $hotelAlias)->first();

        $hotel->name = $request['name'];
        $hotel->owner = $request['owner'];
        $hotel->street = $request['street'];
        $hotel->city = $request['city'];
        $hotel->country = $request['country'];
        $hotel->phone = $request['phone'];
        $hotel->website = $request['website'];
        $hotel->uid = $request['uid'];
        $hotel->bank = $request['bank'];
        $hotel->iban = $request['iban'];
        $hotel->bic = $request['bic'];
        $hotel->save();

        return view('sites.hotel.info', compact('hotel'));
    }
}

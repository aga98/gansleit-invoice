<?php

namespace App\Http\Controllers\module\search;

use App\Http\Controllers\Controller;
use App\Http\Controllers\module\customer\CustomerController;
use App\InvoiceArticle;
use Illuminate\Http\Request;
use App\Invoice;
use App\Customer;
use App\Http\Controllers\ustomer;
use Carbon\Carbon;
use DB;
use PDF;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->CustomerService = new CustomerController();
    }

    public function search(Request $request, $hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();
        $searchBy = $request->input('search');

        $searchResult = [];

        $searchResult['customer'] = Customer::where([['surname', 'LIKE', '%'.$searchBy.'%'],['hotel_id', '=', $hotel->id]])
            ->orWhere([['firstname', 'LIKE', '%'.$searchBy.'%'],['hotel_id', '=', $hotel->id]])
            ->orWhere([['full_name', 'LIKE', '%'.$searchBy.'%'],['hotel_id', '=', $hotel->id]])
            ->orWhere([['city', 'LIKE', '%'.$searchBy.'%'],['hotel_id', '=', $hotel->id]])
            ->orWhere([['plz', 'LIKE', '%'.$searchBy.'%'],['hotel_id', '=', $hotel->id]])
            ->orWhere([['registration_card_number', 'LIKE', '%'.$searchBy.'%'],['hotel_id', '=', $hotel->id]])
            ->get();

        $searchResult['invoice'] = Invoice::where('invoice_id', 'LIKE', '%'.$searchBy.'%')->get();

        return view('sites.search.search', compact('hotel'), compact('searchResult'));
    }
}

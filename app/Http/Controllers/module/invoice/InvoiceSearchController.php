<?php

namespace App\Http\Controllers\module\invoice;

use App\Http\Controllers\Controller;
use App\Http\Controllers\module\customer\CustomerController;
use App\InvoiceArticle;
use Illuminate\Http\Request;
use App\Article;
use App\Http\Controllers\customer;
use Carbon\Carbon;
use DB;
use PDF;

class InvoiceSearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request, $hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();

        return Article::where([['name', 'LIKE', '%'.$request->q.'%'],['hotel_id', '=', $hotel->id]])
            ->orWhere([['name', 'LIKE', '%'.$request->q.'%'],['only_single_hotel', '=', 0]])
            ->get();
    }

    public function searchId(Request $request, $hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();

        return Article::where([['article_id', 'LIKE', '%'.$request->q.'%'],['hotel_id', '=', $hotel->id]])
            ->orWhere([['article_id', 'LIKE', '%'.$request->q.'%'],['only_single_hotel', '=', 0]])
            ->get();
    }
}
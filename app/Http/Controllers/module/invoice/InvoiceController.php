<?php

namespace App\Http\Controllers\module\invoice;

use App\Http\Controllers\Controller;
use App\Http\Controllers\module\customer\CustomerController;
use App\InvoiceArticle;
use Illuminate\Http\Request;
use App\Invoice;
use App\Customer;
use App\Http\Controllers\ustomer;
use Carbon\Carbon;
use DB;
use PDF;

class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->CustomerService = new CustomerController();
    }

    public function viewAll($hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();
        $invoices = DB::table('invoices')->where('hotel_id', $hotel->id)->orderBy('invoice_id', 'desc')->get();

        foreach ($invoices as $invoice) {
            $invoice->customer = DB::table('customers')->where('id', $invoice->customer_id)->first();
        }

        return view('sites.invoice.viewall', compact('hotel'), compact('invoices'));
    }

    public function createIndex($hotelAlias)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();
        $hotel->invoiceId = $this->getInvoiceID($hotel);
        $articles = DB::table('articles')->where('hotel_id', $hotel->id)->get();

        return view('sites.invoice.create', compact('hotel'), compact('articles'));
    }

    public function create($hotelAlias, Request $request)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();
        $request['customer_id'] = $this->CustomerService->create($request);
        $invoice = $this->createInvoice($hotel, $request);
        $this->createInvoiceArticles($invoice, $request, $invoice);

        $invoice = DB::table('invoices')->orderBy('created_at','desc')->first();

        return view('sites.invoice.finish', compact('hotel'), compact('invoice'));
    }

    public function viewPublic($hotelAlias, $invoiceId, Request $request)
    {
        $invoiceId = $invoiceId / 12345678;
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();
        $invoice = DB::table('invoices')->where([['invoice_id', $invoiceId], ['hotel_id', $hotel->id]])->first();
        $articles = DB::table('invoice_articles')->where([['invoice_id', $invoice->invoice_id], ['hotel_id', $invoice->hotel_id]])->get();
        $customer = DB::table('customers')->where('id', $invoice->customer_id)->first();
        $kurtaxe = $invoice->kurtaxe;

        view()->share('customer', $customer);
        view()->share('invoice', $invoice);
        view()->share('kurtaxe', $kurtaxe);
        view()->share('articles', $articles);
        view()->share('hotel', $hotel);

        $pdf = PDF::loadView('sites.invoice.invoice');

        return $pdf->stream('invoice');
    }

    public function view($hotelAlias, $invoiceId, Request $request)
    {
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();
        $invoice = DB::table('invoices')->where([['invoice_id', $invoiceId], ['hotel_id', $hotel->id]])->first();
        $articles = DB::table('invoice_articles')->where([['invoice_id', $invoice->invoice_id], ['hotel_id', $invoice->hotel_id]])->get();
        $customer = DB::table('customers')->where('id', $invoice->customer_id)->first();
        $kurtaxe = $invoice->kurtaxe;

        view()->share('customer', $customer);
        view()->share('invoice', $invoice);
        view()->share('kurtaxe', $kurtaxe);
        view()->share('articles', $articles);
        view()->share('hotel', $hotel);

        $pdf = PDF::loadView('sites.invoice.invoice');

        return $pdf->stream('invoice');
    }

    public function preview(Request $request, $hotelAlias)
    {
        $now = Carbon::now();
        $invoiceArticles = [];
        $hotel = DB::table('hotels')->where('alias', $hotelAlias)->first();

        $invoice = new Invoice();
        $invoice->invoice_id = '0';
        $invoice->aconto = $request->input('aconto');
        $invoice->persons = $request->input('persons');
        $invoice->date_from = $request->input('start');
        $invoice->date_to = $request->input('end');
        $invoice->created_at = $now;

        $customer = new Customer();
        $customer->sex = $request->input('sex');
        $customer->full_name = $request->input('firstname')." ".$request->input('surname');
        if(!empty($request['company'])) {
            $customer->full_name = $request->input('company');
        }
        $customer->plz = $request->input('plz');
        $customer->country = $request->input('country');
        $customer->city = $request->input('city');
        $customer->street = $request->input('street');

        $length = count($request->input('name'));

        for($i = 0; $i < $length;) {
            $invoiceArticle = new InvoiceArticle;

            if($request['article_id'][$i] != null) {
                $article = DB::table('articles')->where('article_id', $request['article_id'][$i])->first();
                $invoiceArticle->invoice_id = $invoice->invoice_id;
                $invoiceArticle->article_id = $request['article_id'][$i];
                $invoiceArticle->name = $request['name'][$i];
                $invoiceArticle->price = $request['price'][$i];
                $invoiceArticle->tax = $request['tax'][$i];
                $invoiceArticle->amount = $request['quantity'][$i];
                $invoiceArticle->discount = $request['discount'][$i];
                $invoiceArticle->total_price = $request['quantity'][$i] * $request['price'][$i];
                $invoiceArticle->tax_second_value = $article->tax_second_value;
                $invoiceArticle->tax_first_percent = $article->tax_first_percent;
                $invoiceArticle->tax_second_percent = $article->tax_second_percent;
                array_push ($invoiceArticles, $invoiceArticle);
            }
            $i++;
        }

        view()->share('customer', $customer);
        view()->share('invoice', $invoice);
        view()->share('articles', $invoiceArticles);
        view()->share('hotel', $hotel);

        $pdf = PDF::loadView('sites.invoice.invoice');

        return $pdf->stream('invoice');
    }

    /**
     * @param $hotel
     * @param $request
     * @param $invoice
     */
    protected function createInvoiceArticles($hotel, $request, $invoice) {

        $length = count($request['quantity']);

        for($i = 0; $i < $length;) {
            $invoiceArticle = new InvoiceArticle;

            if($request['article_id'][$i] != null) {
                if ($request['kurtaxe'][$i] == null) {
                    $invoiceArticle->kurtaxe = 0;
                } else {
                    $invoiceArticle->kurtaxe = $request['kurtaxe'][$i];
                }
                $article = DB::table('articles')->where('article_id', $request['article_id'][$i])->first();
                $invoiceArticle->invoice_id = $invoice->invoice_id;
                $invoiceArticle->hotel_id = $invoice->hotel_id;
                $invoiceArticle->article_id = $request['article_id'][$i];
                $invoiceArticle->name = $request['name'][$i];
                $invoiceArticle->price = $request['price'][$i];
                $invoiceArticle->tax = $request['tax'][$i];
                $invoiceArticle->amount = $request['quantity'][$i];
                $invoiceArticle->total_price = $request['quantity'][$i] * $request['price'][$i];
                $invoiceArticle->tax_second_value = $article->tax_second_value;
                $invoiceArticle->tax_first_percent = $article->tax_first_percent;
                $invoiceArticle->tax_second_percent = $article->tax_second_percent;

                $invoiceArticle->save();
            }
            $i++;
        }
    }

    protected function createInvoice($hotel, $request) {
        $invoice = new Invoice;

        $from = Carbon::parse($request['start'])->format('Ymd');
        $to = Carbon::parse($request['end'])->format('Ymd');

        $invoice->invoice_id = $this->getInvoiceID($hotel);
        $invoice->customer_id = $request['customer_id'];
        $invoice->hotel_id = $hotel->id;
        $invoice->price = 0;
        $invoice->tax = 0;
        $invoice->name = 0;
        $invoice->date_from = $request['start'];
        $invoice->date_to = $request['end'];
        $invoice->aconto = $request['aconto'];
        $invoice->kurtaxe = $to - $from;

        $invoice->save();

        return $invoice;
    }

    protected function getInvoiceID($hotel) {
        $lastInvoice = Invoice::where('hotel_id', '=', $hotel->id)->orderby('created_at', 'desc')->first();
        $now = Carbon::now();
        $currentYear = $now->format('Y');

        if(Invoice::where('hotel_id', '=', $hotel->id)->orderby('created_at', 'desc')->count() == 0) {
            $Id = $now->format('y').sprintf("%04s", 1);
        }
        elseif($currentYear == $lastInvoice->created_at->format('Y')) {
            $Id = sprintf("%04s", $lastInvoice->invoice_id);
            $Id++;
        }
        else {
            $Id = $now->format('y').sprintf("%04s", 1);
        }

        return $Id;
    }
}

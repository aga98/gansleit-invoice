<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'/',  'middleware' => 'auth'], function(){
    Route::get('/', function () {
        return view('home');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'public/{hotelAlias}'], function(){
    Route::get('/invoice-{invoiceId}', 'module\invoice\InvoiceController@viewPublic');
});

Route::group(['prefix'=>'{hotelAlias}',  'middleware' => 'auth'], function(){

    Route::get('/', 'module\hotel\HotelController@index');
    Route::get('/search', 'module\search\SearchController@search');
    Route::post('/', 'module\hotel\HotelController@update');

    Route::group(['prefix'=>'customer'], function(){
        Route::get('/', 'module\customer\CustomerController@viewall');
    });

    Route::group(['prefix'=>'invoice'], function(){
        Route::get('/', 'module\invoice\InvoiceController@viewAll');
        Route::get('/create', 'module\invoice\InvoiceController@createIndex');
        Route::get('/view/{invoiceId}', 'module\invoice\InvoiceController@view');
        Route::get('/preview', 'module\invoice\InvoiceController@preview');
        Route::get('/search', 'module\invoice\InvoiceSearchController@search');
        Route::get('/search-id', 'module\invoice\InvoiceSearchController@searchId');
        Route::post('/create', 'module\invoice\InvoiceController@create');
    });

    Route::group(['prefix'=>'article'], function(){
        Route::get('/', 'module\article\ArticleController@overview');
        Route::post('/create', 'module\article\ArticleController@create');
        Route::post('/update-{articleId}', 'module\article\ArticleController@update');
        Route::get('/destroy-{articleId}', 'module\article\ArticleController@destroy');
    });
});
